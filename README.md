# `hello this is not legal I guess :D`

# TPB scraper

## TODO:

- [ ] check that from list proxy is up
- [ ] pass through proxy or tor network
- [ ] make search based on argument ?
- [ ] return title, size, seeder and magnet link for first 20 lines
- [ ] print to file
- [ ] send to transmission // mix bash script for transmission


### Dependencies:
<br>

Currently moving to using the [Scrapy](https://scrapy.org/) python framework.

To install on **linux**:

```
pip install scrapy
```

### Old Script
#### python dependencies:

python-pip3 is need to install python modules.

```
pip3 install requests requests[socks] argparse bs4 stem
```
apt dependencies:
```
sudo apt install tor
```
edit torrc file to make diffrent ip query:
```
sudo nano /etc/tor/torrc
```
uncomment :

```
ControlPort 9051
## If you enable the controlport, be sure to enable one of these
## authentication methods, to prevent attackers from accessing it.
HashedControlPassword 16:05834BCEDD478D1060F1D7E2CE98E9C13075E8D3061D702F63BCD674DE
```
<br>

---

<br>

### 20210326235747

curl and html2text
specific tags for curl ? why not wget/wget2

`sed -n '/[the pattern itself]/,$p'`

`curl url | html2text`

`curl --socks5-hostname localhost:9050 URL`  // to make a cUrl query with tor as a proxy

### 20210327153320

using python script and apt package tor to scrape...

### 20220204232340

```python
# list of pirate bay proxys that should work...
# TODO:check that they all have the same architecture for html link detection
proxy_list = [
    "https://thepiratebay0.org/",
    "https://thepiratebay10.org/",
    "https://pirateproxy.live/",
    "https://thehiddenbay.com/",
    "https://thepiratebay.zone/",
    "https://tpb.party/"
]
# selecting random proxy link from list
# random line
random_index = random.randrange(len(proxy_list))
# random proxylink in line to variable later used for url to connect to
proxy_link = proxy_list[random_index]
print("> scraping from " + proxy_link)
# generating links to top categories
top = ['100', '101', '102', '103', '104', '199', '200', '201', '202', '203', '204', '205', '206',  '207', '208', '209', '299', '300', '301', '302', '303', '304', '305',  '306', '400', '401', '402', '403', '404', '405', '406', '407', '408', '499', '500', '501', '502', '503', '504', '505', '506', '599', '600', '601', '602', '603', '604', '605', '699']

# loop trhough all l 
for l in top:
    # concatenation of the proxy link and path to top category
    url = proxy_link + 'top/' + l
    # printing message :
    print("> scanning " + url)
```
### 20220205005725

move the `test` folder to your localhost folder (`/var/www` for ubuntu/debian or `/srv/http` for Arch*)

then use the following command to test the local page for html/css element selection

```
scrapy shell http://localhost/testing_tpb/test.html
```
[Scrapy documentation](https://docs.scrapy.org/en/latest/intro/overview.html)