import scrapy

class QuotesSpider(scrapy.Spider):
    name = 'quotes'
    start_urls = [
        'http://localhost/testing_tpb/test.html',
    ]

    def parse(self, response):
        for quote in response.css('div.quote'):
            yield {
                'author': quote.xpath('tr/td:nth-child(2)').get(),
                'text': quote.css('tr.text::text').get(),
            }

        next_page = response.css('a::attr("href")').get()
        if next_page is not None:
            yield response.follow(next_page, self.parse)