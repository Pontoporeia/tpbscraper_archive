# TODO: make a clear description of the project and post in on gitlab ?

import requests
# import argparse
from datetime import datetime
import json
import random
import os
from bs4 import BeautifulSoup
from bs4 import SoupStrainer
from stem import Signal
from stem.control import Controller

print("! starting tpb scraping:\n")
# getting date
now = datetime.now()
# variable used in naming files with appropriate date
global_time = now.strftime("%Y%m%d")
# path to script
# filepath = os.getcwd()
filepath = os.path.dirname(os.path.realpath(__file__))
# print(filepath)	

# creating directories with appropriate dates to house data
# data directory if not there
data_directory = "data"
data_path = os.path.join(filepath,data_directory)
try:
    os.mkdir(data_path)
    print("! Directory" + data_directory + " created"  )
except OSError as error:
    print('> ' + data_directory + 'directory already exists ! skipping ...')
# date directory in data if not there
output_directory = global_time + "_" + 'tpb'
output_path = os.path.join(data_path,output_directory)
try:
    os.mkdir(output_path)
    print("! Directory " + output_directory + " created " )
except OSError as error:
    print('> ' + output_directory + ' already exists. skipping ... \n')

# list of pirate bay proxys that should work...
# TODO:check that they all have the same architecture for link detection
proxy_list = [
    "https://thepiratebay0.org/",
    "https://thepiratebay10.org/",
    "https://pirateproxy.live/",
    "https://thehiddenbay.com/",
    "https://thepiratebay.zone/",
    "https://tpb.party/"
]
# selecting random proxy link from list
# random line
random_index = random.randrange(len(proxy_list))
# random proxylink in line to variable later used for url to connect to
proxy_link = proxy_list[random_index]
print("> scraping from " + proxy_link)
# generating links to top categories
top = ['100', '101', '102', '103', '104', '199', '200', '201', '202', '203', '204', '205', '206',  '207', '208', '209', '299', '300', '301', '302', '303', '304', '305',  '306', '400', '401', '402', '403', '404', '405', '406', '407', '408', '499', '500', '501', '502', '503', '504', '505', '506', '599', '600', '601', '602', '603', '604', '605', '699']

# loop trhough all l 
for l in top:
    # concatenation of the proxy link and path to top category
    url = proxy_link + 'top/' + l
    # printing message :
    print("> scanning " + url)
    
    # tor connection
    def get_tor_session():
        session = requests.session()
        # Tor uses the 9050 port as the default socks port
        session.proxies = {'http':  'socks5://127.0.0.1:9050',
                            'https': 'socks5://127.0.0.1:9050'}
        return session
    # Make a request through the Tor connection
    # IP visible through Tor
    session = get_tor_session()
    # signal TOR for a new connection
    def renew_connection():
        with Controller.from_port(port = 9051) as controller:
            controller.authenticate(password="password")
            controller.signal(Signal.NEWNYM)
    session = get_tor_session()
    # print(session.get("http://httpbin.org/ip").text)

    # i variable used to rank items fro top category
    i = 0

    # getting page from url through tor
    page = session.get(url)
    # page = requests.get(url)

        
    # check status code to see if okay connection
    status = page.status_code
    ok = 200
    if status != ok:
        print("! connection to " + url + " is unavailable")
        exit
    else:
        print('> connection is a go : ' + url)
        # format proxy link url for json with time stamp

        timestamp = now.strftime("%Y%m%d-%H%M%S")

        url_info = {
                'proxy link': proxy_link,
                'time_stamp': timestamp,
                }
        # opening json file
        url_tag = open(output_path + "/" + global_time + "_" + 'tpb_proxy_url.json' , 'w')
        # dumping url_info in url_tag json file
        json.dump(url_info , url_tag, indent = 4)
        # sample = open("ThePirateBay.html", 'r')
        # soup = BeautifulSoup(sample, "html.parser")

        json_file = open(output_path + "/"+ global_time + "_" + 'tpb_'+ l + '.json', 'w')
        # main_json = {
        #     'category'=
        # }
        # json_file.write('{ '+l+': { \n')
        
        # parsing page content
        soup = BeautifulSoup(page.content, "html.parser")
        # selecting second cell in table
        cells = soup.select('tr td:nth-child(2)')
        # for each cell that is second in table

        list_info = []

        for cell in cells:
            # finding links
            links = cell.select('a')
             # if there are less then two links skip
            if len(links) < 2:
                print('too few links, skipping!', cell)
                continue  # skip
            # take only first two links , ignore the rest (in a variable *_ that is forgotten)
            first, second, *_ = links
            # getting the href
            href= second['href']
            # TODO: the description and img selection doesn't work well. To be fixed...
            #getting description
            # na = second.find_next('a')
            # trust = na.find_next('img')
            # trust_level = trust['alt']

            # dsc = trust.find_next('font')
            # selecting the two cells after last link
            td1 = second.find_next('td')
            td2 = td1.find_next('td')
            # incrementing i 
            i = i + 1
            # print(i,first.text, second['href'], td1.txt, td2.txt)
            #  formating for json file
            # print(href)
            # print('         ')
            # href = href.replace('\n','')

            list_info.append ( {
                'rank': str(i),
                'title': first.text,
                'link': href,
                'seeder': td1.text,
                'leecher': td2.text })
            # 'trust level':trust_level,
            # 'description':dsc.text,

            # list_info.append(info)
            # pretty ="\n"
            # print(info)
            # print(json_file)
            # dumping in json file
        
        json.dump(list_info , json_file, indent = 4)
        json_file.write("\n")
        # json.dump(oc , json_file, indent = 4)
    # json_file.write("}")
    print( "> finished scraping " + url + " at " + timestamp + '\n')
